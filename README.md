# EpiContacts - The epically redundant, yet-another-contact-manager code demo

With this repo, and a local docker install, you should be able to get up and running with EpiContacts, one of the greatest ColdFusion-based
contact managers ever built (okay, that's probably a bit much), atop Lucee, the best open-source ColdFusion server, atop OpenLiteSpeed, 
the fastest caching/proxy/web server available, atop Debian, "The Universal Operating System."

Depending on your internet connection speed, you can be up and running in under ten minutes!

## About

Okay, so the world needs another "contacts" app like it needs another... Hello World example.  This repo is more to demonstrate how a CF/Lucee
application with embedded database can be deployed quickly and easily from a git repo. It also has served to demonstrate some of my code
style, for potential clients/employers. Highlights:

* Demonstrates a Dockerfile that pushes OpenLiteSpeed and Lucee install scripts to the container and runs them. These scripts have been refined to configure the
services out of the box to just start up and run.

* Demonstrates, in a way, how NOT to use Docker, as Docker only really shines when microservices are used... in other words, having the H2 database and
the web service all in one container is not exactly "How things are done..."

* Some basic bash scripts are included so you don't have to remember and constantly type Docker commands. They feed entirely off of the vars.sh file
where you configure your desired settings/strings. Similar batch files for Windows will be created in a later release.

* Demonstrates how container data can be persisted in a way that eases development once you're up and running - data, web app files, and logs are all
"under one roof".  For deployment into production, one would probably use volumes instead, and bake the web app files directly into the image/container.

* I chose the Pure CSS library, 'cause I wanted to try something new.

* The app uses a basic home-brewed MVC architecture, with hints from CFWheels. It demonstrates a combination of regular page-to-page interaction (for now)
with a touch of AJAX. Eventually this will likely become a one-page application using an AJAX/UI library of some kind.

## Docker Image: Web Service ("httpd")

### Open Ports

* 80
* 443
* 7080 (OLS Admin)
* 8088 (OLS Example Page/PHP Info)
* 8888 (Lucee Admin)

### Debian v9.3

* http://debian.org

### OpenLiteSpeed v1.4.30 / PHP v7.2.1

* http://open.litespeedtech.com
* http://php.net
* OpenLiteSpeed Admin: http://{SERVER_IP}:7080 - you must use server IP address

### Lucee 5.2.6.060

* http://lucee.org
* Lucee Admin: http://{SERVER_IP}:8888/lucee/admin/server.cfm - you must use server IP address

### From Empty Directory to EpiContacts In Under 10 Minutes

* Presently, there are only scripts available for Linux/MacOS based machines. If you're running Windows, you'll probably need to dissect the .sh scripts and mimick what they
	do manually. (which actually isn't that hard, they are pretty much all DOCKER commands anyway.)

* Install GIT and DOCKER on your system.

* Clone the repository to a directory on your machine

* Set up your local HOSTS file to map www.epicontacts.example to 127.0.0.1

* Run /docker/up.sh.  This will execute a build of the docker container ("httpd") and run it for you

* You should now be able to browse to www.epicontacts.example, and open both the OLS and Lucee administrator sites.

* If you want to add domain aliases (i.e. application set up at example.com but you also want responses at www.example.com) you'll need to add it as follows:

	* In OLS admin: Virtual Host > General > Domain Aliases > add www.example.com

	* Bash-in to httpd, and modify /opt/lucee/tomcat/conf/server.xml : find the host entry block at the bottom of the file, and add the following inside it:
		<Alias>www.example.com</Alias>

* This installation has not been fully hardended for security. Use is at your own risk from here.	

## Your Startup Configuration

### Logging

For your convenience, Docker Compose is configured to bind mount certain log directories in the Docker containers to directories in this project's /https/logs 
directory on the web host. Review the docker-compose.yml file for details.

### Notes:

* Main Lucee web application is available on port 443, and it uses the same SSL certificate as the OLS server. You will need to replace this with a proper purchased SSL certificate
if the public will be accessing the site directly instead of through an SSL-providing reverse proxy like CloudFlare.

* OpenLiteSpeed caching module is installed at the OLS server tier and overridden at the vhost tier. It is configured to expire items after 2 minutes, and is disabled entirely
by default. When caching is enabled, it is persisted for the Docker container as a bind mount in this project's /httpd/cache directory.

* When working with web-server tier settings/configuration, start by seeking to make changes at the vhost tier alone, and work your way up as needed. Priority of settings goes from
vhost context > vhost > server.

* Convenience scripts are available in the /docker directory and its subdirectories, for building, starting, stopping, and removing the docker containers, as well as "bashing in"
to a running container as root; there are also Docker Compose convenience scripts for bringing a stack up or down, and updating images with currently running containers.

* Currently OLS is configured to pass all ColdFusion template requests to Lucee via a "Web Proxy" External App configuration, as there was a bug in OLS that prevented connections
via AJP. The bug has been fixed in the OLS repo, and will be included in this Docker image in a future release.

* OpenLiteSpeed does NOT read .htaccess files. You can copy the rewrite rules of your .htaccess files to the vhost context in the OLS administrator. Be sure to do a graceful restart
of OLS when modifying rewrite rules.  You can browse to OLS Admin > Virtual Hosts > {hostname} > Context Tab > Static context > Rewrite Rules to see the current rules supporting SES
URLs, as an example of where to put 'em. (Yes, not having .htaccess available CAN be a pain, and for apps that rely on the ability to change it, you'll need to adjust how you do things.
That said, it's actually better not to use .htaccess, and store your directives in the server config instead... faster, and more secure. Most apps are set-it-and-forget-it anyway!)

* Patience when restarting Tomcat: if you're doing it manually from inside the container, do the shutdown, then observe via TOP, wait for the java process to die first before
starting back up. If you start back up before the shutdown, the process becomes unstable and you'll get 503s instead of your pages. You'll then need to shut Tomcat down and try again.

* THE DATASOURCE CONFIGURATION, CONNECTING THE APP TO THE H2 DATABASE, DOES NOT YET USE THE SAME STRINGS THAT YOU CONFIGURE IN VARS.SH. They are hard-coded as follows:
	* Datasource name: epicontacts
	* Database name: epicontacts
	* Database user: epicontacts
	* Database password: EpiIsGreat!	
A later version of the app will encrypt the password you provide in vars.sh, and configure the datasource with that password and use the project name you provide in vars.sh
for the source/db/user names.  For now, you can log in to the Lucee admin, create a new datasource however you wish, and use that connection string in Application.cfc.

* Enjoy, and report any issues using the BitBucket issue tracker, please.



