

<cfscript>

	// TODO: convert the rest of this to a single-page app using AJAX and the UI library du jour.

	savecontent variable="request.v.response" {

		switch(request.action) {


			case "list":
		    arrContacts = EntityLoad('Contact');
		    request.v.rsContacts = EntityToQuery(arrContacts);
				request.v.pageTitle = "All Contacts";
				include "views/html/list.cfm";
			break;


			case "search":
				if (!Len(Trim(request.searchTerms))) { location(url="index.cfm", addToken="false") };
				request.v.rsContacts = invoke("models.Contact", "findByCriteria", {criteria=request.searchTerms});
				request.v.pageTitle = "Search Results";
				include "views/html/list.cfm";
			break;


			case "new":
				request.v.objContact = EntityNew("Contact");
				request.v.pageTitle = "New Contact";
				include "views/html/form.cfm";
			break;


			case "newRandom":
				randomContact = application.util.getRandomContact();
				EntitySave(randomContact);
				session.flash.type = "success";
				session.flash.message = "Random contact was created.";
				location(url="index.cfm", addToken="false");
			break;


			case "edit":
				request.v.objContact = EntityLoadByPK("Contact", request.contactId);
				request.v.pageTitle = "Edit Contact";
				include "views/html/form.cfm";
			break;


			case "save":
				if(Len(request.contactId)) {
					c = EntityLoadByPK("Contact", request.contactId);
					session.flash.type = "success";
					session.flash.message = "Contact was updated.";
				} else {
					c = EntityNew("Contact");
					c.setCreatedDateTime(request.timestamp);
					session.flash.type = "success";
					session.flash.message = "Contact was created.";
				}
				c.setFullName(request.fullName);
				c.setPhoneNumber(request.phoneNumber);
				c.setEmailAddress(request.emailAddress);
				c.setModifiedDateTime(request.timestamp);  // TODO: Automate createdDateTime and modifiedDateTime fields.
				session.flash.strErrors = c.validate();
				if(StructCount(session.flash.strErrors)) {
					request.v.pageTitle = "Error";
					session.flash.type = "error";
					session.flash.message = "We found some errors in your submitted data:";
					request.v.objContact = c;
					include "views/html/form.cfm";
				} else {
					EntitySave(c);
					location(url="index.cfm", addToken="false");
				}
			break;


			case "delete":
				doomedContact = EntityLoadByPK("Contact", request.contactId);
				try {
					EntityDelete(doomedContact);
					request.v.event = "CONTACT_DELETED";
					session.flash.type = "success";
					session.flash.message = "Contact was deleted.";

				} catch (any e) {
					request.v.event = "CONTACT_NOT_FOUND";
					session.flash.type = "warning";
					session.flash.message = "Contact was not found.";
				}
				if(request.isAjax) {
					r = CreateObject("component","models.AjaxResponse").init();
					r.event = request.v.event;
					r.addData("contactId", request.contactId);
					request.v.objAjaxResponse = r;
					include "views/json/_ajaxResponse.cfm";
				} else {
					location(url="index.cfm", addToken="false");
				}
			break;


			default:
				include "views/html/error_unknown_action.cfm";
			break;

		}

	}

</cfscript>

