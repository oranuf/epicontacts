

component {

	request.appRoot = GetDirectoryFromPath(GetCurrentTemplatePath());

	// TODO:  Dockerize... this.name, dbName, databasesDir, password all need to come from vars.sh

	this.name = "epicontacts";
	this.applicationTimeout = "#CreateTimespan(1,0,0,0)#";
	this.sessionManagement = "true";
	this.sessionTimeout = "#CreateTimespan(0,0,20,0)#";
	this.setClientCookies = "true";

	// ###############################################################################################################################
	// Embedded database config	
	// ###############################################################################################################################	

	dbName = "epicontacts";
	databasesDir = request.appRoot & "../../db/data";
	dbLocation = "#databasesDir#/#dbName#";	

	this.ormenabled = "true";
	this.ormsettings = { dialect="MySQL", dbCreate="update", logSQL="false" };

	this.datasources["epicontacts"] = {	  class: 'org.h2.Driver'
										, bundleName: 'org.h2'
										, bundleVersion: '1.3.172'
										, connectionString: 'jdbc:h2:#dbLocation#;MODE=MySQL'
										, username: '#dbName#'
										, password: "encrypted:0f986701d6d63ed0ffd4470c2b81ab2eb6365d8f07b7e0f5aa2335e51ab6abf1"
										// optional settings
										, clob:true // default: false
										, connectionLimit:100 // default:-1
										};

	this.datasource = this.datasources[dbName];


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* onApplicationStart()
	*
	* @hint ""
	*
	*/

	boolean function onApplicationStart() {
		include "config/app.cfm";
		include "config/db.cfm";
		return true;
  }


 /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* onSessionStart()
	*
	* @hint ""
	*
	*/

  void function onSessionStart() {
  	// TODO: Flash should really be an object.
  	resetFlash();
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* onRequestStart()
	*
	* @hint ""
	*
	*/

	boolean function onRequestStart(string targetPage) {

		// For convention... any data being passed to a view should be placed in this struct.
		param request.v = StructNew();

		// Used for database timestamps and consistent "now" across entire request
		param request.timestamp = "#DateTimeFormat(Now(), 'yyyy-mm-dd HH:nn:ss.l')#";
		param request.action = "list";

		request.v.flash = session.flash;
		request.isAjax = application.util.isAjaxRequest();

		// copy to common scope
		StructAppend(request, url);
		StructAppend(request, form);

		// ORMReload();

		return true;
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* onRequestEnd()
	*
	* @hint ""
	*
	*/

	void function onRequestEnd(string targetPage) {

		if(request.isAjax) {
			include "views/json/_render.cfm";
		} else {
			include "views/html/_render.cfm";
		}

		resetFlash();

	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* resetFlash()
	*
	* @hint "Creates/clears the flash"
	*
	*/

	private void function resetFlash (  ) {
		session.flash						= {};
		session.flash.type			=	"no-message";
  	session.flash.message		= "";
  	session.flash.strErrors	= {};
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	

}

