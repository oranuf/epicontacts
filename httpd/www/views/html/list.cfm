

<cfparam name="request.v.rsContacts" type="Query">
<cfset rs = request.v.rsContacts>
<cfset _contacts = IIf( rs.RecordCount GT 1, DE("contacts"), DE("contact") )>

<table class="pure-table pure-table-horizontal pure-table-striped contacts-list">

	<cfinclude template="_search.cfm">

	<cfif !rs.RecordCount>

		<caption>
			<cfoutput>
				No records found
				<cfif request.action IS "search">
					matching "#request.searchTerms#"
				</cfif>
			</cfoutput>
		</caption>

	<cfelse>

		<caption>
			<cfoutput>
				Found #rs.RecordCount# #_contacts#
				<cfif request.action IS "search">
					matching "#url.searchTerms#"
				</cfif>
			</cfoutput>
		</caption>

		<cfoutput query="rs">
			<cfset request.v.strContact = application.util.queryRowToStruct(rs, rs.CurrentRow)>
			<cfinclude template="contact.cfm">
		</cfoutput>

	</cfif>

</table>

