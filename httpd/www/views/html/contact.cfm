

<cfparam name="request.v.strContact" type="Struct">
<cfset c = request.v.strContact>

<cfoutput>

	<tbody id="contact_#c.contactId#">

		<tr class="contact">

			<td class="full-name">
				#c.fullName#
			</td>

			<td class="view-details">
				<a href="index.cfm?action=view&contactId=#c.contactId#" class="details-link">
					details</a>
			</td>

			<td class="edit">
				<a href="index.cfm?action=edit&contactId=#c.contactId#" class="edit-link">
					edit</a>
			</td>

			<td class="delete">
				<a href="index.cfm?action=delete&contactId=#c.contactId#" class="delete-link" data-contactid="#c.contactId#">
					delete</a>
			</td>

		</tr>

		<!--- hidden until details clicked --->

		<tr class="contact-details-container" id="yikes">

			<td colspan="4">

				<table class="contact-details">

					<tr>

						<td>
							Phone Number:
						</td>

						<td class="phone-number">
							#c.phoneNumber#
						</td>

					</tr>

					<tr>

						<td>
							Email Address:
						</td>

						<td class="email-address">
							#c.emailAddress#
						</td>

					</tr>

					<tr>

						<td>
							Created:
						</td>

						<td class="created-datetime">
							#DateTimeFormat(c.createdDateTime)#
						</td>

					</tr>

					<cfif IsDate(c.modifiedDateTime)>
						<tr>
							<td>
								Modified:
							</td>
							<td class="modified-datetime">
								#DateTimeFormat(c.modifiedDateTime)#
							</td>
						</tr>
					</cfif>

				</table>

			</td>

		</tr>

	</tbody>

	<cfset StructDelete(request.v,"strContact") >

</cfoutput>

