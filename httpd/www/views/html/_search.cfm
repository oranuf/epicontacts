

<cfparam name="request.searchTerms" default="">


<cfoutput>
	<tr>
		<th colspan="2" class="search-bar">
			<form action="index.cfm" method="GET" class="pure-form">
				<input type="hidden" name="action" value="search">
				<input type="text" name="searchTerms" placeholder="Search" value="#Trim(request.searchTerms)#"
								title="Enter your search criteria and press enter" class="pure-input-rounded pure-input-1">
			</form>
		</th>
		<th colspan="2">
			<a href="index.cfm?action=newRandom">
				<button class="pure-button" title="Click to create a random contact">New Random</button></a>
			<a href="index.cfm?action=new">
				<button class="pure-button pure-button-primary" title="Click to create a new contact">New Contact</button></a>
		</th>
	</tr>
</cfoutput>