

<cfparam name="request.v.flash"						default="{}">
<cfparam name="request.v.flash.message"		default="">
<cfparam name="request.v.flash.strErrors"	default="{}">
<cfparam name="request.v.flash.type"			default="no-message">

<cfset f = request.v.flash>

<cfswitch expression="#f.type#">

	<cfcase value="info">
		<cfset f.icon = "fa-info-circle">
	</cfcase>

	<cfcase value="success">
		<cfset f.icon = "fa-check">
	</cfcase>

	<cfcase value="warning">
		<cfset f.icon = "fa-warning">
	</cfcase>

	<cfcase value="error">
		<cfset f.icon = "fa-times-circle">
	</cfcase>

	<cfdefaultcase>
		<cfset f.icon = "fa-address-book-o">
	</cfdefaultcase>

</cfswitch>


<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->


<cfoutput>

	<div class="centered maxWidth flash flash-#f.type#">

		<a href="##" class="flash-closer"><i class="fa fa-window-close-o"></i></a>

		<i class="fa #f.icon#"></i>

		#f.message#

		<cfif StructCount(f.strErrors)>
			<ul>
				<cfloop collection="#f.strErrors#" item="errorMessage">
			   <li>#f.strErrors[errorMessage]#</li>
				</cfloop>
			</ul>
		</cfif>

	</div>

</cfoutput>


