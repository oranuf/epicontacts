

<cfparam name="request.v.objContact">
<cfparam name="request.v.strErrors" default="">
<cfset c = request.v.objContact>

<cfoutput>

	<div class="centered maxWidth insetBorder">

	<form action="index.cfm" method="post" class="pure-form pure-form-aligned" style="border:1px solid silver;">

		<input type="hidden" name="action" value="save">
		<input type="hidden" name="contactId" value="#c.getContactId()#">

		<fieldset>

			<div class="pure-control-group">
				<label for="fullName">Full Name:</label>
				<input name="fullName" id="fullName" type="text" value="#c.getFullName()#" autofocus="true" minlength="1" placeholder="fname lname" required class="pure-u-3-5">
				<span class="pure-form-message-inline">This is a required field.</span>
			</div>


			<div class="pure-control-group">
				<label for="phoneNumber">Phone Number:</label>
				<input name="phoneNumber" id="phoneNumber" type="text" value="#c.getPhoneNumber()#" minlength="12" placeholder="xxx-xxx-xxxx" pattern="^[0-9]{3}-[0-9]{3}-[0-9]{4}$" required class="pure-u-3-5">
				<span class="pure-form-message-inline">This is a required field.</span>
			</div>


			<div class="pure-control-group">
				<label for="emailAddress">Email Address:</label>
				<input name="emailAddress" id="emailAddress" type="email" value="#c.getEmailAddress()#" minlength="5" placeholder="address@example.com" required class="pure-u-3-5">
				<span class="pure-form-message-inline">This is a required field.</span>
			</div>


			<div class="pure-controls textRight">
				<button type="button" name="userAction" value="cancel" class="pure-button cancelButton">Cancel</button>
				<button type="submit" name="userAction" value="saveContact" class="pure-button pure-button-primary" formnovalidate="true">Save Contact</button>
			</div>

		</fieldset>

	</form>

	</div>

</cfoutput>

