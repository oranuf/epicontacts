


<cfparam name="request.v.pageTitle" default="[No Page Title Specified]" type="String">
<cfparam name="request.v.browserTitle" default="#request.v.pageTitle#" type="String">

<cfsavecontent variable="request.v.header">

	<cfoutput>

		<!doctype html>

		<html lang=en>

			<head>

				<meta charset=utf-8>

				<title>
					#request.v.browserTitle# - EpiContacts
				</title>

				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" type="text/css" href="stylesheets/pure-release-0.6.2/pure-min.css"/>
				<link rel="stylesheet" href="stylesheets/main.css">

				<script src="javascripts/jquery-3.2.1.min.js"></script>
				<script src="https://use.fontawesome.com/ab33d382b2.js"></script>
				<script src="javascripts/main.js"></script>

			</head>

			<body>

				<header class="centered maxWidth">
					<h1 class="page-title">#request.v.pageTitle#</h1>
					<a href="index.cfm"><img alt="EpiContacts" width="568" height="131" src="images/logo.png" class="pure-img"></a>
				</header>

				<cfinclude template="_flash.cfm">

	</cfoutput>

</cfsavecontent>

