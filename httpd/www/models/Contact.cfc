

component extends="Model" persistent="true" entityname="Contact" table="Contacts" {

	property name="contactId" fieldtype="id" generator="increment" displayName="Contact ID";

	property name="fullName"					type="string" required="true" displayName="Full Name"					length="256"	default=""	;
	property name="phoneNumber"				type="string" required="true" displayName="Phone Number"			length="16" 	default=""	;
	property name="emailAddress"			type="string" required="true" displayName="Email Address"			length="256" 	default=""	;

	property name="createdDateTime"		ormtype="timestamp"	required="true" displayName="Created On"				default=""	;
	property name="modifiedDateTime"	ormtype="timestamp"	required="true" displayName="Last Modified On"	default=""	;


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* init()
	*
	* @hint "Constructor"
	*
	*/

	public function init() {
		this.initDateTime = Now();
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* validate()
	*
	* @hint "Validates all properties required for proper save"
	*
	*/

	public Struct function validate() {

		local.strErrors = {};

		if( ListLen(getFullName(), " ") LT 2 ) {
			local.strErrors.fullName = "You must enter your full name in the format FNAME {space} LNAME.";
		}

		if(!IsValid("regex",getPhoneNumber(), "^[0-9]{3}-[0-9]{3}-[0-9]{4}$")) {
			local.strErrors.phoneNumber = "You must enter a phone number in the format xxx-xxx-xxxx.";
		}

		if(!IsValid("Email",getEmailAddress())) {
			local.strErrors.emailAddress = "You must enter a valid email address in the format myAddress@example.com";
		}

		return local.strErrors;
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* findByCriteria()
	*
	* @hint "Retrieves contacts based on the passed criteria"
	*
	*/

	public Query function findByCriteria( String criteria="" ) {

		local.sql = '	SELECT
										*
									FROM
										Contacts
									WHERE
										UPPER(fullName) LIKE :criteria
											OR
										UPPER(phoneNumber) LIKE :criteria
											OR
										UPPER(emailAddress) LIKE :criteria
									ORDER BY
										fullName
								';

		local.rs = QueryExecute(	local.sql,
															{ criteria = "%#UCase(arguments.criteria)#%" }
														);

		return local.rs;
	}


/* ---------------------------------------------------------------------------------------------------------------------------*/


}

