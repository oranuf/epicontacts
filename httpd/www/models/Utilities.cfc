

component extends="model" {


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* getRandomContact()
	*
	* @hint "Retrieves a random name and generates random phone number and email address."
	*
	*/

	public models.Contact function getRandomContact() {

		local.contactProperties = {};
		local.contactProperties.fullName				= getRandomName();
		local.contactProperties.phoneNumber			= getRandomPhoneNumber();
		local.contactProperties.emailAddress		= getRandomEmailAddress(local.contactProperties.fullName);
		local.contactProperties.createdDateTime = request.timestamp;
		local.c = EntityNew("Contact", local.contactProperties);
		return local.c;

	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* getRandomName()
	*
	* @hint "Returns random name."
	*
	*/

	public String function getRandomName() {
		local.pos = RandRange(1, ArrayLen(application.arrRandomContacts));
		local.randomName = Trim(application.arrRandomContacts[local.pos]);
		return local.randomName;
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* getRandomPhoneNumber()
	*
	* @hint "Returns random phone number."
	*
	*/

	public String function getRandomPhoneNumber() {
		local.r1 = RandRange(100, 999);
		local.r2 = RandRange(100, 999);
		local.r3 = RandRange(1000,9999);
		local.randomPhoneNumber = "#local.r1#-#local.r2#-#local.r3#";
		return local.randomPhoneNumber;
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* getRandomEmailAddress()
	*
	* @hint "Returns random email address."
	*
	*/

	public String function getRandomEmailAddress(String name) {
		local.emailName = Replace(Trim(arguments.name), " ", "", "all");
		local.emailUnique = RandRange(1000,9999);
		local.randomEmailAddress = "#local.emailName##local.emailUnique#@example.com";
		return local.randomEmailAddress;
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* queryRowToStruct()
	*
	* @hint "Given a query and row number, returns a struct containing the data in that row."
	*
	*/

	public struct function queryRowToStruct(query, rowNumber) {
		local.i=0;
		local.rowData = StructNew();
		local.cols = ListToArray(arguments.query.columnList);
		for (local.i=1; i LTE ArrayLen(local.cols); local.i++) {
			rowData[local.cols[local.i]] = query[local.cols[i]][arguments.rowNumber];
		}
		return rowData;
	}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	*
	* isAjaxRequest()
	*
	* @hint "Returns true if the current request was made via AJAX."
	*
	*/

	public Boolean function isAjaxRequest() {
		local.headers = getHttpRequestData().headers;
		return StructKeyExists(local.headers, "X-Requested-With") AND (local.headers["X-Requested-With"] IS "XMLHttpRequest");
	}


/* ---------------------------------------------------------------------------------------------------------------------------*/


}

