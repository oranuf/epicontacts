

component displayname="models.AjaxResponse" output="false" hint="Standard format object for AJAX JSON responses" {

	public function init() {
		this.event = "";
		this.data = structNew();
		this.html = structNew();
		return this;
	}

	public void function addData(String name, Any value) {
		StructInsert(this.data, arguments.name, arguments.value);
	}


	public any function getData(String key) {
		local.data = this["data"]["#arguments.key#"];
		return local.data;
	}

	public void function addHtml(String name, Any html){
		StructInsert(this.html, arguments.name, arguments.html);
	}

	public String function getHtml(String key){
		local.html = this["html"]["#arguments.key#"];
		return local.html;
	}

}

