

<cfscript>

	// If database has ZERO records, it's brand new; add sample records and whatever else
	// you might want to do to a fresh database here.

	arrNewDbCheck = EntityLoad('Contact');

	if (!ArrayLen(arrNewDbCheck)) {

		strSampleContact = {
			fullName = "John Doe",
			phoneNumber = "888-555-1212",
			emailAddress = "jdoe@example.com",
			createdDateTime = '1970-02-1 21:00:00.123'
		};

		sampleContact = EntityNew( "Contact", strSampleContact );

		EntitySave(sampleContact);
		OrmFlush();

		application.dtmDbConfig = Now();

	}

</cfscript>


