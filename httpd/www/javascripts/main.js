

$(document).ready(function() {


	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	$('.details-link').on('click', function(e) {
		e.preventDefault();
		$(this).closest('tbody').find( '.contact-details-container td' ).slideToggle('slow');
	});
	
	
	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	
	$('.cancelButton').on('click', function(){
		window.location.replace('index.cfm');
	})
	
	
	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	
	$('.delete-link').on('click', function(e) {
	
		e.preventDefault();
		
		if(confirm("Are you sure you wish to delete this contact?")) {
			
			_contactId = $(this).data('contactid');
			
			$.ajax({
				type: 'POST',	
				url: 'index.cfm',	
				dataType: 'json',	
				data: {
					action: 'delete',
					contactId: _contactId
				},	
				success: function(response){
					$('.flash').hide();
		    		evt = response.EVENT;
		    		if(evt == 'CONTACT_DELETED'){
		    			_contactId = response.DATA.contactId;
		    			doomedDiv = '#contact_' + _contactId;
		    			$(doomedDiv).fadeOut('slow');
		    		} else {
		    			alert('Error deleting contact: ' + evt);
		    		}
				}	
			});		
						
		}		
	})
	
	
	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	
	$('.flash-closer').on('click', function(e) {
		$('.flash').slideToggle('slow');		
	})	
	
	
	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	// TODO:  create a fadeout timer if the flash is showing
	
	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	
});

