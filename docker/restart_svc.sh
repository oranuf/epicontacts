#!/bin/bash

source ./vars.sh

if [ "x$1" = "x" ] ; then
	echo
	echo " Please specify which service you wish to restart: ols | lucee  "
	echo
	exit 1
fi

case $1 in       

    ols )			docker exec -d ${ATCS_PROJECT_NAME}_httpd_1 /usr/local/lsws/bin/lswsctrl restart
					;;

   lucee )			docker exec -d ${ATCS_PROJECT_NAME}_httpd_1 /opt/lucee/lucee_ctl restart
					;;
  
esac

