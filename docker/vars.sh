#!/bin/bash

## Project name:  short name for httpd service, such as brand name ("mycompany") or generic ("publicweb").
## 	(Project name becomes basis for Docker network names, etc)
## App domain:  full primary domain name upon which the web service will respond
## Client code:  client code/account number (basis for multiple domains under one client folder, if you're hosting that way)
## Password: Becomes password for all services admins/logins

export ATCS_PROJECT_NAME=epicontacts
export ATCS_APP_DOMAIN=www.epicontacts.example
export ATCS_CLIENT_CODE=atcs001
export ATCS_CLIENT_PASSWORD="EpiIsGreat!"