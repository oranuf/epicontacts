#!/bin/bash

SERVER_ROOT=/opt/lucee
SERV_CONFIG_FILE=${SERVER_ROOT}/tomcat/conf/server.xml
ADMIN_PASSWD=
VALIDATED_PASSWORD=
VHOST_DOMAIN=example.com
FOLLOWPARAM=

# ############################################################################################################

function usage {
    echoY "USAGE:                           " "$0 [options] [options] ..."
    echoY "OPTIONS                          "
    echoG "  --adminpassword (-a) PASSWORD   " "To set the webAdmin password for openlitespeed instead of using a random one."        
    echoG "  --help (-h)                    " "To display usage."    
    echoG "  --appdomain (-d)               " "The primary domain under which the web application will run (e.g. www.example.com); use * (default) to support running under any domain name."
    echo
}

function echoY {
    FLAG=$1
    shift
    echo -e "\033[38;5;148m$FLAG\033[39m$@"
}

function echoG {
    FLAG=$1
    shift
    echo -e "\033[38;5;71m$FLAG\033[39m$@"
}

function echoR {
    FLAG=$1
    shift
    echo -e "\033[38;5;203m$FLAG\033[39m$@"
}

function check_value_follow {
    FOLLOWPARAM=$1
    local PARAM=$1
    local KEYWORD=$2

    #test if first letter is - or not.
    if [ "x$1" = "x-n" ] || [ "x$1" = "x-e" ] || [ "x$1" = "x-E" ] ; then
        FOLLOWPARAM=
    else
        local PARAMCHAR=`echo $1 | awk '{print substr($0,1,1)}'`
        if [ "x$PARAMCHAR" = "x-" ] ; then
            FOLLOWPARAM=
        fi
    fi

    if [ "x$FOLLOWPARAM" = "x" ] ; then
        if [ "x$KEYWORD" != "x" ] ; then
            echoR "Error: '$PARAM' is not a valid '$KEYWORD', please check and try again."
            usage
            exit 1
        fi
    fi
}

function validate_password {
    if [ "x$1" != "x" ] ; then
        VALIDATED_PASSWORD=$1
    else
        local RAND=$RANDOM
        local DATE0=`date`
        VALIDATED_PASSWORD=`echo "$RAND0$DATE0" |  md5sum | base64 | head -c 8`
    fi
}

function display_banner {
    echo
    echoY '**********************************************************************************************'
    echoY '*                  Lucee installation for Debian 9 (Stretch) and OpenLiteSpeed               *'
    echoY '*                              Copyright (C) 2018  Advantex LLC                              *'   
    echoY '**********************************************************************************************'
    echoY '===================================     ' ${1} '     ==================================='
    echo
}

#####################################################################################
####   Exec
#####################################################################################

export JRE_HOME=/opt/lucee/jdk/jre/

while [ "$1" != "" ]; do
    case $1 in      

    	-a | --adminpassword )      check_value_follow "$2" ""
                                    if [ "x$FOLLOWPARAM" != "x" ] ; then
                                        shift
                                    fi
                                    ADMIN_PASSWD=$FOLLOWPARAM
                                    ;; 

        -d | --appdomain )          check_value_follow "$2" ""
                                    if [ "x$FOLLOWPARAM" != "x" ] ; then
                                        shift
                                    fi
                                    VHOST_DOMAIN=$FOLLOWPARAM
                                    ;;                                    

        -h | --help )               usage
                                    exit 0
                                    ;;

        * )                         usage
                                    exit 0
                                    ;;
    esac
    shift
done

validate_password "${ADMIN_PASSWD}"
ADMIN_PASSWD=$VALIDATED_PASSWORD

display_banner "BEGIN"

echo
echoR "Installing Lucee to $SERVER_ROOT with the following parameters:"
#echoY "Lucee runtime user:       " "nobody"
echoY "WebAdmin password:        " "${ADMIN_PASSWD}"
echoY "App Domain:               " "$VHOST_DOMAIN"
echo

cd ~

wget --progress=dot:giga http://cdn.lucee.org/lucee-5.2.6.060-pl0-linux-x64-installer.run

chmod +x lucee-5.2.6.060-pl0-linux-x64-installer.run

echoR "NOW STARTING LUCEE INSTALL IN UNATTENDED MODE; Please stand by, with the patience of Job..."
./lucee-5.2.6.060-pl0-linux-x64-installer.run --mode unattended --luceepass "${ADMIN_PASSWD}" --installconn false 
#--systemuser nobody

cat > ${VHOST_DOMAIN}.xml <<EOF
	<!-- inserted by Dockerfile -->
	<Host name="${VHOST_DOMAIN}" appBase="webapps" unpackWARs="true" autoDeploy="true">
		<Context path="" docBase="/srv/${VHOST_DOMAIN}/httpd/www">
			<JarScanner scanClassPath="false"/>
		</Context>
	</Host>
EOF

sed -i -e '/<!-- ADD NEW HOSTS HERE -->/r '"${VHOST_DOMAIN}.xml" ${SERV_CONFIG_FILE}

mkdir /srv/${VHOST_DOMAIN}/db
mkdir /srv/${VHOST_DOMAIN}/db/data

display_banner "END"
