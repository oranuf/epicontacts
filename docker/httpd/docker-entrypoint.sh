#!/bin/bash

# permissions checks
chown -R lsadm:lsadm /usr/local/lsws/conf
chown -R nobody:nogroup /srv/${ATCS_APP_DOMAIN}/httpd/cache
chown -R nobody:nogroup /srv/${ATCS_APP_DOMAIN}/httpd/www
chown -R nobody:nogroup /usr/local/lsws/logs

export JRE_HOME=/opt/lucee/jdk/jre/
/opt/lucee/tomcat/bin/startup.sh
/usr/local/lsws/bin/lswsctrl start
tail -F /usr/local/lsws/logs/access.log
