#!/bin/bash

SERVER_ROOT=/usr/local/lsws
SERV_CONFIG_FILE=${SERVER_ROOT}/conf/httpd_config.conf
EMAIL=

ADMIN_PASSWD=
VALIDATED_PASSWORD=

#All lsphp versions, keep using two digits to identify a version!!!
LSPHP_VER_LIST=(54 55 56 70 71 72)
LSPHP_VER=7.2
USEDEFAULTLSPHP=1

VHOST_DOMAIN=example.com

ALLERRORS=0
FOLLOWPARAM=

# ############################################################################################################


function echoY {
    FLAG=$1
    shift
    echo -e "\033[38;5;148m$FLAG\033[39m$@"
}


function echoG {
    FLAG=$1
    shift
    echo -e "\033[38;5;71m$FLAG\033[39m$@"
}


function echoR {
    FLAG=$1
    shift
    echo -e "\033[38;5;203m$FLAG\033[39m$@"
}


function check_root {
    local INST_USER=`id -u`
    if [ ${INST_USER} != 0 ] ; then
        echoR "Sorry, only the root user can install."
        echo
        exit 1
    fi
}


function display_banner {
    echo
    echoY '**********************************************************************************************'
    echoY '*                     OpenLiteSpeed installation for Debian 9 (Stretch)                      *'
    echoY '*                              Copyright (C) 2018  Advantex LLC                              *'   
    echoY '**********************************************************************************************'
    echoY '===================================     ' ${1} '     ==================================='
    echo
}


function config_dirs {
    APP_ROOT=/srv/${VHOST_DOMAIN}
        SVC_ROOT=${APP_ROOT}/httpd
            LOG_ROOT=${SVC_ROOT}/logs
            DOC_ROOT=${SVC_ROOT}/www
            CAC_ROOT=${SVC_ROOT}/cache
}


function install_ols {

    grep -Fq  "http://rpms.litespeedtech.com/debian/" /etc/apt/sources.list.d/lst_debian_repo.list
    if [ $? != 0 ] ; then
        echo "deb http://rpms.litespeedtech.com/debian/ stretch main"  > /etc/apt/sources.list.d/lst_debian_repo.list
    fi

    wget -O /etc/apt/trusted.gpg.d/lst_debian_repo.gpg http://rpms.litespeedtech.com/debian/lst_debian_repo.gpg
    wget -O /etc/apt/trusted.gpg.d/lst_repo.gpg http://rpms.litespeedtech.com/debian/lst_repo.gpg

    apt-get -y update
    apt-get -y install openlitespeed

    apt-get -y install lsphp${LSPHP_VER} lsphp${LSPHP_VER}-mysql lsphp${LSPHP_VER}-imap lsphp${LSPHP_VER}-curl

    if [ "x${LSPHP_VER}" != "x70" ] && [ "x${LSPHP_VER}" != "x71" ] && [ "x${LSPHP_VER}" != "x72" ] ; then
        apt-get -y install lsphp${LSPHP_VER}-gd lsphp${LSPHP_VER}-mcrypt
    else
       apt-get -y install lsphp${LSPHP_VER}-common lsphp${LSPHP_VER}-json
    fi

    if [ $? != 0 ] ; then
        echoR "An error occured during openlitespeed installation."
        ALLERRORS=1
    else
        ln -sf ${SERVER_ROOT}/lsphp${LSPHP_VER}/bin/lsphp ${SERVER_ROOT}/fcgi-bin/lsphp5
    fi
}


function config_server {

    mkdir ${APP_ROOT}
    mkdir ${SVC_ROOT}
    mkdir ${LOG_ROOT}
    mkdir ${DOC_ROOT}
    mkdir $CAC_ROOT}

    touch ${DOC_ROOT}/index.htm
    touch ${DOC_ROOT}/index.cfm    

    mv /tmp/*.png ${DOC_ROOT}

    chown -R nobody:nogroup ${APP_ROOT}

    cat >> ${DOC_ROOT}/index.htm <<END
    <h1>Hello from Litespeed vhost ${VHOST_DOMAIN}</h1>
    <p> Static file for installation testing. </p>
    <img src="ols_1.4.30.png">
    <img src="lucee_5.2.6.060.png">

END

    cat >> ${DOC_ROOT}/index.cfm <<END
    <cfoutput>
        <h1>Hello from Litespeed vhost ${VHOST_DOMAIN}</h1>
        <p> Dynamic file for installation testing: <strong> #DateTimeFormat(Now())# </strong> </p>
        <img src="ols_1.4.30.png">
        <img src="lucee_5.2.6.060.png">
    </cfoutput>
END


    if [ -e "${SERV_CONFIG_FILE}" ] ; then
        cat ${SERV_CONFIG_FILE} | grep "virtualhost ${VHOST_DOMAIN}" >/dev/null
        if [ $? != 0 ] ; then

            # SERVER LEVEL SETTINGS:
            
            # Server > General > General > "Use Client IP in Header"
            sed -i -e "s/useIpInProxyHeader/useIpInProxyHeader  1\n# useIpInProxyHeader/" "${SERV_CONFIG_FILE}"

            # Server > General > General > "Administrator Email"
            sed -i -e "s/adminEmails/adminEmails $EMAIL\n# adminEmails/" "${SERV_CONFIG_FILE}"

            # Server > Tuning > Connection > "Smart Keep-Alive"
            sed -i -e "s/smartKeepAlive/smartKeepAlive  1\n# smartKeepAlive/" "${SERV_CONFIG_FILE}"            
            
            HOST_CONFIG_FILE=${SERVER_ROOT}/conf/vhosts/${VHOST_DOMAIN}/vhconf.conf

            cat >> ${SERV_CONFIG_FILE} <<END

virtualhost ${VHOST_DOMAIN} {
    vhRoot                  ${APP_ROOT}
    configFile              ${HOST_CONFIG_FILE}
    allowSymbolLink         0
    enableScript            1
    restrained              0
    maxKeepAliveReq         1000
    smartKeepAlive          1
    setUIDMode              2
    staticReqPerSec         50
    dynReqPerSec            5
    outBandwidth            100000
    inBandwidth             50000
}

listener ${VHOST_DOMAIN}:80 {
    address                 *:80
    secure                  0
    map                     ${VHOST_DOMAIN} *
}

listener ${VHOST_DOMAIN}:443 {
    address                 *:443
    secure                  1
    keyFile                 $SERVER_ROOT/admin/conf/webadmin.key
    certFile                $SERVER_ROOT/admin/conf/webadmin.crt
    map                     ${VHOST_DOMAIN} *
}

module cache {
enableCache                    0
enablePrivateCache        0
checkPublicCache            1
checkPrivateCache          1
qsCache                             1
reqCookieCache               1
ignoreReqCacheCtrl         1
ignoreRespCacheCtrl       0
respCookieCache              1
expireInSeconds                3600
privateExpireInSeconds    3600
maxStaleAge                        200
maxCacheObjSize               10000000
storagepath                          cachedata
noCacheDomain                       
noCacheUrl                            
no-vary                                  0
addEtag                                 0
}


END

            mkdir -p $SERVER_ROOT/conf/vhosts/${VHOST_DOMAIN}/
            cat > $HOST_CONFIG_FILE <<END

docRoot                   $DOC_ROOT/

errorlog $LOG_ROOT/error.log {
  useServer               0
  logLevel                INFO
  rollingSize             64M
}

accesslog $LOG_ROOT/access.log {
  useServer               0
  rollingSize             64M
  keepDays                60
  compressArchive         1
  logFormat               %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\
  
}

index  {
  useServer               0
  indexFiles              index.cfm
}

scripthandler  {
  add                     proxy:Lucee cfm
  add                     proxy:Lucee cfml
  add                     proxy:Lucee cfc
  add                     proxy:Lucee lucee
}

extprocessor Lucee {
  type                    proxy
  address                 127.0.0.1:8888
  maxConns                1000
  pcKeepAliveTimeout      15
  initTimeout             60
  retryTimeout            15
  respBuffer              0
}

context / {
  type                    null
  location                ${DOC_ROOT}
  allowBrowse             1
  enableExpires           1
  expiresDefault          A120
  extraHeaders            <<<END_extraHeaders
x-open-the-door-its-dave: dave's not here, man
  END_extraHeaders


  indexFiles              index.cfm

  rewrite  {
    enable                1
    inherit               1
  }
  addDefaultCharset       off
}


rewrite  {
  enable                  1
  logLevel                0
}

module cache {
enableCache                      0
enablePrivateCache               0
checkPublicCache                 1
checkPrivateCache                1
qsCache                          1
reqCookieCache                   1
ignoreReqCacheCtrl               1
ignoreRespCacheCtrl              0
respCookieCache                  1
expireInSeconds                  10
privateExpireInSeconds           10
maxStaleAge                      300
maxCacheObjSize                  10000000
storagepath                      ${SVC_ROOT}/cache
no-vary                          0
addEtag                          1

}

END

# NOTE: some resources for creating the rewrite rules above, for use in various situations:
# https://www.litespeedtech.com/support/wiki/doku.php/litespeed_wiki:cache:no-plugin-setup-guidline
# https://www.litespeedtech.com/support/wiki/doku.php/litespeed_wiki:cache:no-plugin-advanced:purge (possibly)

            chown -R lsadm:lsadm ${SERVER_ROOT}/conf/
        fi

        #set up OLS admin password
        ENCRYPT_PASS=`"${SERVER_ROOT}/admin/fcgi-bin/admin_php" -q "${SERVER_ROOT}/admin/misc/htpasswd.php" ${ADMIN_PASSWD}`
        if [ $? = 0 ] ; then
            echo "admin:${ENCRYPT_PASS}" > "${SERVER_ROOT}/admin/conf/htpasswd"
            if [ $? = 0 ] ; then
                echoY "Finished setting OpenLiteSpeed webAdmin password to ${ADMIN_PASSWD}."
                echoY "Finished updating server configuration."

            else
                echoY "OpenLiteSpeed webAdmin password not changed."
            fi
        fi
    else
        echoR "${SERVER_ROOT}/conf/httpd_config.conf is missing, it seems that something went wrong during openlitespeed installation."
        ALLERRORS=1
        exit 1
    fi
}


function validate_password {
    if [ "x$1" != "x" ] ; then
        VALIDATED_PASSWORD=$1
    else
        local RAND=$RANDOM
        local DATE0=`date`
        VALIDATED_PASSWORD=`echo "$RAND0$DATE0" |  md5sum | base64 | head -c 8`
    fi
}


function check_value_follow {
    FOLLOWPARAM=$1
    local PARAM=$1
    local KEYWORD=$2

    #test if first letter is - or not.
    if [ "x$1" = "x-n" ] || [ "x$1" = "x-e" ] || [ "x$1" = "x-E" ] ; then
        FOLLOWPARAM=
    else
        local PARAMCHAR=`echo $1 | awk '{print substr($0,1,1)}'`
        if [ "x$PARAMCHAR" = "x-" ] ; then
            FOLLOWPARAM=
        fi
    fi

    if [ "x$FOLLOWPARAM" = "x" ] ; then
        if [ "x$KEYWORD" != "x" ] ; then
            echoR "Error: '$PARAM' is not a valid '$KEYWORD', please check and try again."
            usage
            exit 1
        fi
    fi
}


function usage {
    echoY "USAGE:                           " "$0 [options] [options] ..."
    echoY "OPTIONS                          "
    echoG "  --adminpassword (-a) PASSWORD   " "To set the webAdmin password for openlitespeed instead of using a random one."
    echoG "  --email (-e) EMAIL             " "To set the email of the server administrator."
    echoG "  --lsphp VERSION                " "To set the version of lsphp, such as 56.  Supported versions: '${LSPHP_VER_LIST[@]}'."
    echoG "  --help (-h)                    " "To display usage."
    echoG "  --appname (-n)                 " "Virtual host / web application name (e.g. www-example-com)"
    echoG "  --appdomain (-d)               " "The primary domain under which the web application will run (e.g. www.example.com); use * (default) to support running under any domain name."
    echo
}


function test_page {
    local URL=$1
    local KEYWORD=$2
    local PAGENAME=$3

    rm -rf tmp.tmp
    wget --no-check-certificate -O tmp.tmp  $URL >/dev/null 2>&1
    grep "$KEYWORD" tmp.tmp  >/dev/null 2>&1

    if [ $? != 0 ] ; then
        echoR "Error: $PAGENAME failed."
    else
        echoG "OK: $PAGENAME passed."
    fi
    rm tmp.tmp
}


function test_ols {
    # NOTE: by default on setup, the virtual domain responds on localhost, so temporary mod
    # of HOSTS file for the ${VHOST_DOMAIN} is not required to do these tests    
    test_page https://localhost:7080/ "LiteSpeed WebAdmin" "webAdmin page"
    test_page http://localhost:8088/  "Congratulations" "Example vhost page"
    #test_page http://localhost/index.htm  "Hello" "${VHOST_DOMAIN} static home page"
}


#####################################################################################
####   Exec
#####################################################################################

export DEBIAN_FRONTEND=noninteractive

display_banner "BEGIN"

while [ "$1" != "" ]; do
    case $1 in       

        -a | --adminpassword )      check_value_follow "$2" ""
                                    if [ "x$FOLLOWPARAM" != "x" ] ; then
                                        shift
                                    fi
                                    ADMIN_PASSWD=$FOLLOWPARAM
                                    ;;

        -e | --email )              check_value_follow "$2" "email address"
                                    shift
                                    EMAIL=$FOLLOWPARAM
                                    ;;

        --lsphp )                   check_value_follow "$2" "lsphp version"
                                    shift
                                    cnt=${#LSPHP_VER_LIST[@]}
                                    for (( i = 0 ; i < cnt ; i++ ))
                                    do
                                        if [ "x$1" = "x${LSPHP_VER_LIST[$i]}" ] ; then
                                            LSPHP_VER=$1
                                            USEDEFAULTLSPHP=0
                                        fi
                                    done
                                    ;;       

        -d | --appdomain )          check_value_follow "$2" ""
                                    if [ "x$FOLLOWPARAM" != "x" ] ; then
                                        shift
                                    fi
                                    VHOST_DOMAIN=$FOLLOWPARAM
                                    ;;                                    

        -h | --help )               usage
                                    exit 0
                                    ;;

        * )                         usage
                                    exit 0
                                    ;;
    esac
    shift
done

check_root

if [ "x$EMAIL" = "x" ] ; then
    if [ "x$VHOST_DOMAIN" = "x*" ] ; then
        EMAIL=admin@localhost
    else
        EMAIL=admin@$VHOST_DOMAIN
    fi
fi

validate_password "${ADMIN_PASSWD}"
ADMIN_PASSWD=$VALIDATED_PASSWORD

echo
echoR "Installing OpenLiteSpeed to $SERVER_ROOT/ with the following parameters:"
echoY "lsphp version:            " "${LSPHP_VER}"
echoY "WebAdmin email:           " "$EMAIL"
echoY "WebAdmin password:        " "${ADMIN_PASSWD}"
echoY "App Domain:               " "$VHOST_DOMAIN"
echo

config_dirs

install_ols

#write the password file for record and remove the previous file.
echo "WebAdmin password is [${ADMIN_PASSWD}]." > $SERVER_ROOT/password

config_server

$SERVER_ROOT/bin/lswsctrl stop >/dev/null 2>&1
$SERVER_ROOT/bin/lswsctrl start

chmod 600 "$SERVER_ROOT/password"
echoY "Please be aware that your password was written to file '$SERVER_ROOT/password'."

if [ "x$ALLERRORS" = "x0" ] ; then
    echoG "Congratulations! Installation finished."
    echoY "NOTE:  OLS supports mod_rewrite rules, but NOT within .htaccess"
    echoY "You will need to copy your .htaccess rules into the server or virtual host rewrite rules via the OLS admim ($VHOST_DOMAIN):7080"
else
    echoR "Installation finished. Some errors seem to have occured, please check this as you may need to manually fix them."
fi
echo

echoY "Testing ..."
test_ols

display_banner "END"

