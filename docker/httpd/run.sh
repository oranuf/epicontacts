#!/bin/bash

source ../vars.sh

echo ===========================================================================================
docker container prune -f
docker run -d --name ${ATCS_PROJECT_NAME}_httpd_1 -p 80:80 -p 443:443 -p 7080:7080 -p 8088:8088 ${ATCS_PROJECT_NAME}-httpd
