#!/bin/bash

source ../vars.sh

clear
echo ===========================================================================================
docker build -t ${ATCS_PROJECT_NAME}-httpd \
			 --build-arg ATCS_PROJECT_NAME=${ATCS_PROJECT_NAME} \
			 --build-arg ATCS_APP_DOMAIN=${ATCS_APP_DOMAIN} \
			 --build-arg ATCS_CLIENT_CODE=${ATCS_CLIENT_CODE} \
			 --build-arg ATCS_CLIENT_PASSWORD=${ATCS_CLIENT_PASSWORD} \
			.




